var gameApp = angular.module('gameApp', []);


gameApp.controller('TodoCtrl', ['$scope', function ($scope) {
}]);



gameApp.controller('TodoCtrl2', ['$scope', "$http", function ($scope, $http) {
    $http.get('data.json').success(function(data) {
        $scope.data = data;
        // console.log("[data] :", $scope.data);
    });

    var btn = document.getElementById("btn");
    var txtSpan = document.getElementById("text");
    // $scope.txts = {"ok": ["BINGO! YOU WON!", "ok"], "wrong": ["Sorry. Try Again!", "wrong"]};
    $scope.txt = "";

// console.log($scope.selectedOpt);

    $scope.foo = function() {
        btn.removeAttribute("disabled");
    }

    $scope.click = function() {
        var c=document.getElementById("canvas");
        var ctx=c.getContext("2d");
        ctx.clearRect(0, 0, c.width, c.height);
        // var img=document.getElementById("img");
        var img=document.createElement("img");
        var imgUrl = getRandomImg()[0];
        img.setAttribute("src", imgUrl);
        
        if(_.invert($scope.data)[imgUrl] == $scope.selectedOpt) {
            // console.log("[BINGO] : YOU WON!", $scope.selectedOpt);
            $scope.txt = "BINGO! YOU WON!";
            txtSpan.removeAttribute("class");
            txtSpan.setAttribute("class", "win");
        }
        else {
            // console.log("[SORRY] : TRY AGAIN!", $scope.selectedOpt);
            txtSpan.removeAttribute("class");
            txtSpan.setAttribute("class", "wrong");
            $scope.txt = "Sorry. Try Again.";
        }

        img.onload = function() {
            ctx.drawImage(img, 10, 10);
        
            // Здесь прятать не надо, т.к. если захочет повторить
            // btn.setAttribute("disabled", "disabled");
            img = null;
        };
        btn.removeAttribute("disabled");
    }


    /**
     * [getRandomImg description]
     * @return {[type]} [description]
     */
    function getRandomImg() {
        return _.sample($scope.data, 1);
    };

    // console.log("[data] :", $scope.data);
    btn.setAttribute("disabled", "disabled");
}]);