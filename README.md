This is a Test Work.

The game imitates a fruit machine.
You choose a fruit in <select>, then press a [Spielen!] button and start to hope that the chosen image will appear.

Technologies used:

Node.js as a back-end, Angularjs as a front-end, Bootstrap 3 for CSS styles, <canvas> element to show fruit images, NO jQuery

To run the program, just clone the repo, cd to "/game" directory, run "node bin/www" and visit "http://localhost:3000/" in your browser. 

You need to have Node.js instaled.